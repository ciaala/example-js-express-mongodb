# Example Backend Project : NodeJS + Express + MongoDB 

The app is reachable on port 3001
Shows a list of registered animals and allows to insert new animals via a html form. 

## Express 5.0.0-alpha

Uses *ejs* as templating language
Uses *body-parser* to parse the post payload

## Nodejs

Uses *nodemon* to restart the server during development  (as devDependencies)

## MongoDB

Runs inside a docker container
There's a shell script with the commands to run to pull the docker image and start it

## Documentation

### Introduction to Express & MongoDB

- [Part 1: Create - Read](https://zellwk.com/blog/crud-express-mongodb/)
- [Part 2: Update - Delete](https://zellwk.com/blog/crud-express-and-mongodb-2/)
- [More](https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4)

### NodeJS + Database Integration

Among the options

- Cassandra
- Couchbase
- CouchDB
- LevelDB
- MySQL
- MongoDB
- Neo4j
- Oracle
- PostgreSQL
- Redis
- SQL Server
- SQLite
- ElasticSearch

https://expressjs.com/en/guide/database-integration.html#neo4j

### Express API

- https://expressjs.com/en/5x/api.html

### MongoDB on Docker

- https://hub.docker.com/_/mongo
- https://docs.mongodb.com/manual/reference/program/mongo/

### GraphQL Introduction

- https://www.hypergraphql.org/tutorial/

### GraphQL + ApolloClient

- http://dev.apollodata.com/react/cache-updates.html

### Framework: Feathers

- https://docs.feathersjs.com/

### Template

- [Handlebars + Webpack](https://www.youtube.com/watch?v=wSNa5b1mS5Y)

### Markdown

- [Atlassian Documentation](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html#Markdownsyntaxguide-Codeblocks)
