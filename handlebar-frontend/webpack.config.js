const path = require('path');

module.exports = {
  entry: path.join(__dirname, '/src/js/main.js'),
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'frontend_bundled.js',
  },
  resolve: {
    alias: {
      Templates: path.join(__dirname, 'src/template'),
      CSS: path.join(__dirname, 'src/css'),
    },
  },
  module: {
    rules: [
      {test: /\.hbs$/, loader: 'handlebars-loader'},
    //  {test: /\.css$/, loader: ['css-loader', 'style-loader']},
    ],
  },
  mode: 'development',
};
console.log(JSON.stringify(module.exports, null, 2));
