export class AnimalsAPI {
  constructor() {
    this.url = '/api/animals';
  }

  /**
   * Transform an object to a human readable JSON in string format
   * @param {object} object
   * @return {string}
   */
  static stringify(object) {
    return JSON.stringify(object, null, 2);
  }

  get(name, callback) {
    fetch(this.url + '/' + _id, {
      method: 'GET',
      //     body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((response) => response.json()).then((response) => {
      console.debug('Success: ', AnimalsAPI.stringify(response));
      callback(response);
    }).catch((error) => console.error('Error:', error));
  }

  insert({family, name, description}, callback) {
    fetch(this.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({family, name, description}),
    }).then((response) => response.json()).then((response) => {
      console.debug('Success: ', AnimalsAPI.stringify(response));
      this.loadAll(callback);
    }).catch((error) => console.error('Error: ', error));
  }

  update({_id, family, name, description}, callback) {
    fetch(this.url + '/' + _id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({_id, family, name, description}),
    }).then((response) => response.json()).then((response) => {
      console.debug('Success: ', AnimalsAPI.stringify(response));
      callback(response);
    }).catch((error) => console.error('Error: ', error));
  }

  /**
   *
   * @param {string}_id
   * @param {function(object)} callback
   */
  delete(_id, callback) {
    if (window.confirm('Are you sure you want to delete ?')) {
      fetch(this.url + '/' + _id, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((response) => response.json()).then((response) => {
        console.debug('Success: ', AnimalsAPI.stringify(response));
        if (callback) {
          callback(response);
        }
      }).catch((error) => console.error('Error: ', error));
    }
  }

  load(_id, callback) {
    fetch(this.url + '/' + _id, {
      method: 'GET',
    }).then((response) => response.json()).then(function(response) {
      console.debug('Success: ', AnimalsAPI.stringify(response));
      if (callback) {
        callback(response);
      }
    }).catch((error) => console.error('Error: ', error));
  }

  loadAll(callback) {
    fetch(this.url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((response) => response.json()).then((response) => {
      console.debug('Success: ', AnimalsAPI.stringify(response));
      if (callback) {
        callback(response);
      }
    }).catch((error) => console.error('Error: ', error));
  }
}
