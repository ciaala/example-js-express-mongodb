export class Observable {

  constructor() {
    this.observers = {};
  }

  unsubscribe(event, notifier) {
    if (event && this.observers[event]) {
      this.observers[event] = this.observers[event].filter(notifier);
    }
  }

  subscribe(event, notifier) {
    if (this.observers) {
      if (!this.observers[event]) {
        this.observers[event] = [notifier];
      } else {
        if (this.observers[event].length > 0) {
          this.unsubscribe(event, notifier);
        }
        this.observers[event].push(notifier);
      }
    }
  }

  notify(event, data) {
    if (event) {
      for (const notifier of this.observers[event]) {
        const consume = notifier({event, data});
        if (consume) {
          break;
        }
      }
    }
  }
}
