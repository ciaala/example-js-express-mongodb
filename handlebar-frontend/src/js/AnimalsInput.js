import animalsInputTmpl from 'Templates/animalsInput.hbs';
import {Observable} from './core/Observable';

export class AnimalsInput extends Observable {
  constructor(parentNode, api) {
    super();
    this.parentNode = parentNode;
    this.api = api;
    this.isEdit = false;
  }

  /**
   * return the value of the form input field matching the 'name'
   * @param {string} name
   * @return {string}
   */
  getInputValueByName(name) {
    const input = this.form.querySelector('input[name=\'' + name + '\']');
    return input ? input.value : undefined;
  }

  setInputValueByName(name, value) {
    const input = this.form.querySelector('input[name=\'' + name + '\']');
    if (input) {
      input.value = value ? value : '';
    } else {
      window.alert('Unable to find the form element with name: ' + name);
    }
  }

  submit() {
    const payload = {
      identifier: this.getInputValueByName('name'),
      description: this.getInputValueByName('description'),
      family: this.getInputValueByName('family'),
    };
    this.notify('submit', {
      family: payload.family,
      name: payload.identifier,
      description: payload.description,
    });
  }

  render() {
    this.parentNode.innerHTML = animalsInputTmpl();
    this.form = this.parentNode.querySelector('form');

    const submitButton = this.form.querySelector(
        'button[data-id=\'submit\']');
    submitButton.onclick = this.submit.bind(this);
    this.form.querySelector(
        'button[data-id=\'clear\']').onclick = this.clear.bind(this);
    this.updateSubmitButtonText();
  }

  clear() {
    this.setInputValueByName('name', '');
    this.setInputValueByName('description', '');
    this.setInputValueByName('family', '');
    this.notify('clear', {});
  }

  loadForm(data) {
    if (data) {
      this.setInputValueByName('name', data.name);
      this.setInputValueByName('description', data.description);
      this.setInputValueByName('family', data.family);
    } else {
      console.error('loadForm called with null data');
    }
  }

  updateSubmitButtonText() {
    const submitButton = this.form.querySelector(
        'button[data-id=\'submit\']');
    submitButton.textContent = this.isEdit ? 'Update' : 'Insert';
  }

  setEditMode(isEdit) {
    this.isEdit = isEdit;
    this.updateSubmitButtonText();
  }
}
