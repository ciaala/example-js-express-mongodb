import {AnimalsAPI} from './AnimalsAPI';
import {AnimalsTable} from './AnimalsTable';
import {AnimalsInput} from './AnimalsInput';

class AnimalsUI {
  constructor(parentNode) {
    this.isEdit = false;
    this.api = new AnimalsAPI();
    this.parentNode = parentNode;
    this.prepareUI();
  }

  prepareUI() {
    this.table = document.createElement('div');
    this.parentNode.append(this.table);
    this.input = document.createElement('div');
    this.parentNode.append(this.input);
    this.animalsTable = new AnimalsTable(this.table, this.api);
    this.animalsInput = new AnimalsInput(this.input, this.api);
    this.animalsTable.subscribe('edit', this.edit.bind(this));
    this.animalsTable.subscribe('delete', this.delete.bind(this));
    this.animalsInput.subscribe('clear', this.clear.bind(this));
    this.animalsInput.subscribe('submit', this.insert.bind(this));

  }

  clear() {
    this.isEdit = false;
    this.animalsInput.setEditMode(this.isEdit);
  }

  render() {
    try {
      this.animalsInput.render();
    } catch (e) {
      console.error(e);
    }
    try {
      this.animalsTable.render();
    } catch (e) {
      console.error(e);
    }
  }

  edit({event, data}) {
    const _id = data;
    if (event !== 'edit') {
      console.error('unexpected event', event);
    }
    this.api.load(_id, function(data) {
      this.isEdit = true;
      this.animalsInput.setEditMode(this.isEdit);
      this._idInEdit = data._id;
      this.animalsInput.loadForm(data);
    }.bind(this));
  }

  delete({event, data}) {
    const _id = data;
    if (event !== 'delete') {
      console.error('unexpected event');
    }
    const args = [
      _id,
      function(data) {
        console.log('callback delete');
        this.animalsTable.render();
        // todo add eventual real callback
      }.bind(this),
    ];
    this.api.delete.apply(this.api, args);
  }

  insert({event, data}) {
    if (event !== 'submit') {
      console.error('unexpeted event received', event);
      return;
    }
    if (!this.isEdit) {
      const args = [
        data, function(data) {
          console.log('callback insert');
          this.animalsTable.render();
          // todo add eventual real callback
        }.bind(this)];
      this.api.insert.apply(this.api, args);
    } else {
      data['_id'] = this._idInEdit;
      const args = [
        data,
        function(data) {
          console.log('callback update');
          this.animalsTable.render();
          // todo add eventual real callback
        }.bind(this)];
      this.api.update.apply(this.api, args);
    }
  }

  start() {
    this.render();
  }
}

export default AnimalsUI;
