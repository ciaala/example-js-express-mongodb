import animalsTableTmpl from 'Templates/animalsTable.hbs';
import {Observable} from './core/Observable.js';

export class AnimalsTable extends Observable {
  constructor(parentNode, api) {
    super();
    this.parentNode = parentNode;
    this.api = api;
  }

  delete(event) {
    let rowId= this.extractElementId(event, 'delete');

    console.log('rowId', rowId);
    this.notify('delete', rowId);
  }

  extractElementId(event, className) {
    let row = event.target;
    while ( row  && (!row.className || !row.className.includes(className))) {
      row = row.parentNode;
    }
    if (row) {
      return row.getAttribute('data-extra');
    }
    return null;
  }
  edit(event) {
    let rowId= this.extractElementId(event, 'edit');
    console.log('rowId', rowId);
    this.notify('edit', rowId);
  }

  render() {
    this.api.loadAll(function(data) {
      //console.log(data);
      this.parentNode.innerHTML = animalsTableTmpl({animals: data});

      const element = this.parentNode.querySelector('ul');
      //console.log(element);
      const rows = element.querySelectorAll('li');
      for (let i = 0; i < rows.length; i++) {
        const row = rows[i];
        row.querySelector('.edit').onclick = this.edit.bind(this);
        row.querySelector('.delete').onclick = this.delete.bind(this);
      }
    }.bind(this));
  }
}
