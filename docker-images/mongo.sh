#!/usr/bin/env bash

docker pull mongo

docker rm mongodb
docker run -d \
-p 27017:27017 -p 28017:28017 \
--name mongodb \
-v $HOME/projects/example-js-express-mongodb/run/db:/data/db \
mongo

docker logs -f mongodb
