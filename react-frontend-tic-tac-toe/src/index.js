import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Square extends React.Component {
  render() {
    return (
        <button className="square" onClick={() => this.props.onClick()}>
          {this.props.value}
        </button>
    );
  }
}

class Board extends React.Component {

  renderSquare(i) {
    return <Square value={this.props.boardTiles[i]}
                   onClick={() => this.props.onClick(i)}/>;
  }

  render() {
    return (
        <div>
          <h1>Tile Game</h1>
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
        </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          boardTiles: Array(9).fill(null),
        }],
      stepNumber: 0,
      player: 'X'
    };
  }
  jumpTo(step) {
    this.setState({
      stepNumber: step,
      player: ((step % 2) === 0) ? 'X' : 'O'
    });
  }
  getCurrentBoardTiles() {
    return this.state.history[this.state.stepNumber].boardTiles;
  }

  handleClick(index) {
    if (this.state.stepNumber !== (this.state.history.length -1)
        || calculateWinner(this.getCurrentBoardTiles())) {
      return;
    }
    const currentBoardTiles = this.getCurrentBoardTiles().slice();

    if (index < currentBoardTiles.length && currentBoardTiles[index] === null) {
      currentBoardTiles[index] = this.state.player;
      this.setState({
        history: this.state.history.concat([{boardTiles: currentBoardTiles}]),
        player: this.state.player === 'X' ? 'O' : 'X',
        stepNumber: this.state.history.length,
      });
    }
  }

  render() {
    const currentBoardTiles = this.getCurrentBoardTiles();
    const winner = calculateWinner(currentBoardTiles);

    const moves = this.state.history.map((step, move) => {
      const desc = move ?
          'Go to move #' + move :
          'Go to game start';
      return (
          <li>
            <button onClick={() => this.jumpTo(move)}>{desc}</button>
          </li>
      );
    });

    const status = winner ?
        'Winner: ' + winner :
        'Next Player: ' + this.state.player;
    return (
        <div className="game">
          <div className="game-board">
            <Board
                boardTiles={currentBoardTiles}
                onClick={i => this.handleClick(i)}/>
          </div>
          <div className="game-info">
            <div>{status}</div>
            <ol>{moves}</ol>
          </div>
        </div>
    );
  }
}

// ========================================

ReactDOM.render(
    <Game/>,
    document.getElementById('root'),
);

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}
