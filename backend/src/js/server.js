const express = require('express');
const path = require('path');
const ObjectID = require('mongodb').ObjectID;

const mongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const AnimalsService = require('./AnimalsService');
const cors = require('cors');

function prepareRouter(app, express, mongoClient, ObjectID) {
  const service = new AnimalsService(app, express, mongoClient, ObjectID);
  return service.getRouter();
}

/**
 * Create a json human readable
 * @param {object} data the object to transform to JSON
 * @return {string} a JSON in string format human readable
 */
function stringify(data) {
  return JSON.stringify(data, null, 2);
}

function main(express, mongoClient, ObjectID, port) {
  const app = express();
  //
  // API Endpoints
  //
  app.use(bodyParser.json());
  const router = prepareRouter(app, express, mongoClient, ObjectID);
  app.use('/api', cors(), router);

  //
  // Handlebar
  //
  app.use(express.static(path.join(__dirname, '../views/')));
  app.use('/handlebar/js',
      express.static(
          path.join(__dirname,
              '..', '..', '..', 'handlebar-frontend',
              'dist')));
  app.use('/handlebar/css', express.static(path.join(__dirname, '../css/')));

  //
  // Tic Tac Toe with React
  //
  app.use('/react-ttt', express.static(
      path.join(__dirname, '..', '..', '..',
          'react-frontend-tic-tac-toe', 'build'))
  );

  app.use('/react', express.static(
      path.join(__dirname, '..', '..', '..',
          'react-crud', 'build'))
  );
  const versions = {'port': port, 'express': app.locals};
  console.log('Starting backend express: ' + stringify(versions));

  app.listen(port, ()=>{
     console.log(arguments);
  });
  console.log('end of the world');
}

const port = process.env.PORT || 3001;

main(express, mongoClient, ObjectID, port);
