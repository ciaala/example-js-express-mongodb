function stringify(object) {
  return JSON.stringify(object, null, 2);
}

class AnimalsService {

  /**
   *
   * @param app {express.App}
   * @param express {express}
   * @param mongoClient {mongodb.MongoClient}
   * @constructor
   */
  constructor(app, express, mongoClient, ObjectID) {
    this.router = express.Router();
    this.mongoClient = mongoClient;
    this.ObjectID = ObjectID;
    this.init();
  }

  insert(request, response) {
    this.mongoClient.connect('mongodb://localhost:27017/',
        function(err, client) {
          if (err) throw err;
          const db = client.db('animals');
          db.collection('animals').insertOne(request.body, (error, result) => {
            if (error) console.error('Error in insert', err);
            console.log(request.method, result);
            response.json(
                {status: 'ok', message: 'created animal' + request.body.name});
          });
        });
  }

  getById(request, response) {
    const _id = request.params['animal_id'];
    if (!_id) {
      return;
    }
    this.mongoClient.connect('mongodb://localhost:27017/',
        function(err, client) {
          if (err) throw err;
          const db = client.db('animals');

          db.collection('animals').
              findOne({'_id': new this.ObjectID(_id)}).
              then(function(object) {
                console.log(request.method, request.path, object);
                response.json(object);
              }).
              catch(function(error) {
                if (error) throw error;
              });
        }.bind(this));
  }

  deleteById(request, response) {
    this.mongoClient.connect('mongodb://localhost:27017/',
        function(err, client) {
          if (err) throw err;
          const db = client.db('animals');
          const _id = request.params.animal_id;
          db.collection('animals').
              deleteOne({'_id': new this.ObjectID(_id)}).
              then(function(object) {
                console.log(request.method, stringify(object));
                response.json(object);
              }).
              catch(function(error) {
                if (error) throw error;
              });
        }.bind(this));
  }

  update(request, response) {
    console.log('UPDATE animals', request);
    this.mongoClient.connect('mongodb://localhost:27017/',
        function(err, client) {
          if (err) throw err;
          const db = client.db('animals');
          const _id = request.params.animal_id;
          db.collection('animals').
              updateOne(
                  {'_id': new this.ObjectID(_id)},
                  {
                    $set: {
                      family: request.body.family,
                      name: request.body.name,
                      description: request.body.description,
                    },
                  },
                  {upsert: true}).
              then(function(object) {
                response.json(object);
              }).
              catch(function(error) {
                if (error) throw error;
              });
        }.bind(this));
  }

  getList(request, response) {
    this.mongoClient.connect('mongodb://localhost:27017/',
        function(err, client) {
          if (err) throw err;
          const db = client.db('animals');
          db.collection('animals').find().toArray(function(err, result) {
            if (err) throw err;
            console.log(request.method, result);
            response.json(result);
          });
        });
  }

  index(request, response) {
    response.json({message: 'hooray! welcome to our api!'});
  }

  init() {
    this.router.post('/animals', this.insert.bind(this));
    this.router.get('/animals/:animal_id', this.getById.bind(this));
    this.router.delete('/animals/:animal_id', this.deleteById.bind(this));
    this.router.put('/animals/:animal_id', this.update.bind(this));
    this.router.get('/animals', this.getList.bind(this));
    this.router.get('/', this.index.bind(this));
  }

  getRouter() {
    return this.router;
  }
}

module.exports = AnimalsService;
