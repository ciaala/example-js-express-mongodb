import React from 'react';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import EditIcon from '@material-ui/icons/EditOutlined';
import DeleteIcon from '@material-ui/icons/DeleteOutlineRounded';

import * as PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import {Link} from 'react-router-dom';

const customStyle = theme => ({
  textFragment: {
    maxWidth: 250,
    minWidth: 250,
    width: 250,
  },

});

class CrudListItem extends React.Component {

  handleOnEdit(event) {
    event.preventDefault();
    this.props.ebus.publish(
        {
          event,
          action: 'EDIT',
          type: 'ANIMAL',
          data: {id: this.props.id},
        },
    );
  }

  handleOnDelete(event) {
    event.preventDefault();
    this.props.ebus.publish(
        {
          event,
          action: 'DELETE',
          type: 'ANIMAL',
          data: {id: this.props.id},
        },
    );
  }

  render() {
    return (
        <React.Fragment
            key={'fragment-' + this.props.id}>
          {(this.props.id % 10 === 0) &&
          <ListSubheader key={'subheader-' + this.props.id}
                         className={this.props.className}>{this.props.id} - {this.props.id +
          10}</ListSubheader>}
          <ListItem key={'item-' + this.props.id}>
            <Link to={`/animals/${this.props.id}`}>
              <IconButton><EditIcon/></IconButton>
            </Link>
            {Array.isArray(this.props.text) &&
            this.props.text.map((fragment, index) =>
                <ListItemText
                    key={'text-' + this.props.id + index}
                    primary={fragment}
                    className={this.props.classes.textFragment}
                />)
            }
            {!Array.isArray(this.props.text) &&
            <ListItemText primary={this.props.text}/>}
            <IconButton onClick={event => this.handleOnDelete(event)}>
              <DeleteIcon />
            </IconButton>
          </ListItem>
        </React.Fragment>
    );
  }
}

CrudListItem.propTypes = {
  classes: PropTypes.object.isRequired,
  ebus: PropTypes.object.isRequired,
};

export default withStyles(customStyle)(CrudListItem);






