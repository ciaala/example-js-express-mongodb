import React from 'react';
import Button from '@material-ui/core/Button';

import {TextField, withStyles} from '@material-ui/core';
import * as PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';

const useStyles = theme => ({
  formElement: {
    margin: 10,
  },

  inputText: {
    width: 640,
    padding: 10,
  },
  button: {
    marginLeft: 20,
  },
});

class AnimalForm extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      family : this.props.family,
      description: this.props.description,
      name: this.props.name,
    };
  }
  render() {
    const textFieldClasses = [
      this.props.classes.inputText,
      this.props.classes.formElement].join(' ');
    const buttonClasses = [
      this.props.classes.formElement,
      this.props.classes.button].join(' ');
    const buttonText = this.props.buttonText !== undefined ?
        this.props.buttonText :
        'Animal Create';
    return (<React.Fragment>
          <List>
            <ListItem>
              <TextField label='family' variant='outlined'
                         value={this.state.family}
                         className={textFieldClasses}
                         onChange={event => {
                           this.setState({family: event.target.value});
                         }}/></ListItem>
            <ListItem><TextField label='name' variant='outlined'
                                 value={this.state.name}
                                 className={textFieldClasses}
                                 onChange={event => {
                                   this.setState({name: event.target.value});
                                 }}/></ListItem>
            <ListItem><TextField label='description' variant='outlined'
                                 value={this.state.description}
                                 className={textFieldClasses}
                                 onChange={event => {
                                   this.setState(
                                       {description: event.target.value});
                                 }}/></ListItem>
            <ListItem><Button className={buttonClasses}
                              variant={'outlined'}
                              onClick={this.onSubmit.bind(this)}
            >{buttonText}</Button></ListItem>

          </List>
        </React.Fragment>
    );
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.onSubmit({
      family: this.state.family,
      description: this.state.description,
      name: this.state.name,
    });
  }
}

AnimalForm.propTypes = {
  classes: PropTypes.object.isRequired,
  buttonText: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  description: PropTypes.string.isRequired,
  family: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default withStyles(useStyles)(AnimalForm);
