import React, {Component} from 'react';
import {AppBar} from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import * as PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export class CrudAppBar extends Component {

  render() {
    return <AppBar className={this.props.classes.appBar}>
      <Toolbar>
        <Link to={`/settings`}>
          <IconButton edge="start" color="inherit" aria-label="open drawer">
            <MenuIcon/>
          </IconButton>
        </Link>
        <Link to={`/`}>
          <IconButton edge="start" color="inherit" aria-label="home">
            <HomeIcon/>
          </IconButton>
        </Link>
        <Fab color="secondary" onClick={this.props.onAdd} aria-label="add"
             className={this.props.classes.fabButton}>
          <AddIcon/>
        </Fab>
      </Toolbar>
    </AppBar>;
  }
}

CrudAppBar.propTypes = {
  classes: PropTypes.any.isRequired,
  onAdd: PropTypes.func.isRequired,
};
