import React from 'react';
import * as PropTypes from 'prop-types';
import AnimalForm from './AnimalForm';

class AnimalCreate extends React.Component {
  state = {
    family: '',
    name: '',
    description: '',
  };

  render() {
    return (
        <AnimalForm classes={this.props.classes}
                    onSubmit={this.props.onSubmit.bind(this)}
                    buttonText={'Create'}
                    description={this.state.description}
                    family={this.state.family}
                    name={this.state.family}
        />
    );
  }

  onSubmit(event) {
    this.props.onSubmit(this.state);
  }
}

AnimalCreate.propTypes = {
  classes: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default AnimalCreate;
