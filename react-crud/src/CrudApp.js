import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import {Router, Route, Switch} from 'react-router-dom';
import * as PropTypes from 'prop-types';
import {createHashHistory} from 'history';
import 'typeface-roboto';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {CrudAppBar} from './CrudAppBar';
import AnimalList from './AnimalList';
import {AnimalsAPI} from './AnimalsAPI';
import AnimalCreate from './AnimalCreate';
import AnimalDetail from './AnimalDetail';
import {EventBus} from './EventBus';
import './index.css';

const useStyles = theme => ({
  text: {
    padding: theme.spacing(2, 2, 0),
  },
  paper: {
    paddingBottom: 50,
  },
  list: {
    marginBottom: theme.spacing(2),
  },
  subheader: {
    backgroundColor: theme.palette.background.paper,
  },
  appBar: {
    top: 'auto',
    bottom: 0,
  },
  grow: {
    flexGrow: 1,
  },
  fabButton: {
    position: 'absolute',
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: '0 auto',
  },
});

class CrudApp extends React.Component {
  state = {
    animals: [],
    history: createHashHistory({
      hashType: 'noslash' // Omit the leading slash
    }),
  };

  constructor(props) {
    super(props);
    this.api = new AnimalsAPI(
        'http://ciaala.internet-box.ch:3001/api/animals');
    this.ebus = new EventBus();
    this.registerEventListeners();
  }

  registerEventListeners() {
    const type = 'ANIMAL';
    this.ebus.subscribe(type, 'UPDATE', this.handleUpdate.bind(this));
    this.ebus.subscribe(type, 'DELETE', this.handleDelete.bind(this));
  }
  createAnimal (form) {
    console.log(form);
    this.api.insert(form, () =>
        this.state.history.push('/')
    );
  }

  handleUpdate(type, action, data) {
    console.log({type,action,data});
    this.api.update(data, (payload) => {
      console.log(payload);
      this.state.history.push('/');
    });
  }

  handleDelete(type, action, data) {
    console.log({type, action, data});
    this.api.delete(data['id'], (payload) => {
      this.ebus.publish({type:'ANIMAL',action:'DELETED', data:payload});
    });
    return false;
  }

  toCreate() {
    this.state.history.push('/animals/new');
  }

  render() {
    return (
        <React.Fragment>
          <CssBaseline/>
          <Paper square className={this.props.classes.paper}>
            <Typography className={this.props.classes.text}
                        align='center'
                        gutterBottom
                        variant='h1'>CrudApp</Typography>
            <Router history={this.state.history} >
              <Switch history={this.state.history}>
                <Route path='/' exact history={this.state.history}
                       render={(...props)=> (<AnimalList {...props}
                                                         ebus={this.ebus}
                                                         classes={this.props.classes}
                                                         api={this.api}
                       />)}/>
                <Route path="/animals/new"  history={this.state.history} >
                  <AnimalCreate classes={this.props.classes}
                                onSubmit={this.createAnimal.bind(this)}/></Route>
                <Route path="/animals/:animalId" history={this.state.history}>
                  <AnimalDetail classes={this.props.classes}
                                ebus={this.ebus}
                                api={this.api}/></Route>
              </Switch>
              <CrudAppBar classes={this.props.classes} onAdd={this.toCreate.bind(this)}/>
            </Router>
          </Paper>
        </React.Fragment>
    );
  }
}

CrudApp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(useStyles)(CrudApp);
