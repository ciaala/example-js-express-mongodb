import React from 'react';

import List from '@material-ui/core/List';
import CrudListItem from './CrudListItem';
import * as PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import ListItem from '@material-ui/core/ListItem';

class AnimalList extends React.Component {
  constructor(props) {
    super(props);
    this.unmounted = false;
    this.listener = this.props.ebus.subscribe('ANIMAL', 'DELETED', this.loadAllAnimals.bind(this));
  }

  state = {
    animals: [],
  };

  componentDidMount() {
    console.debug('Mount AnimalList');
    const location = this.props.history.location.pathname;
    if (location === '/') {
      this.loadAllAnimals();
    }
  }

  componentWillUnmount() {
    console.debug('Unmount AnimalList');
    this.unmounted = true;
    this.props.ebus.unsubscribe('ANIMAL','DELETED', this.listener);
  }

  loadAllAnimals() {
    console.debug('AnimalList loadAllAnimals', this);

    if ( ! this.unmounted) {
      this.props.api.loadAll((data) => this.setState({animals: data}));
    } else {
      console.debug('Already unmounted ', this);
    }
  }

  render() {
    const hasLoadedAnimals = this.state.animals && this.state.animals.map;
    if (hasLoadedAnimals) {
      return (
          <List component='ul' className={this.props.classes.list}>
            {this.state.animals.map(({_id, name, family, description}) => (
                <CrudListItem key={_id}
                              ebus={this.props.ebus}
                              className={this.props.classes.subheader}
                              text={[name, family, description]}
                              id={_id}/>
            ))
            }
          </List>
      );
    } else {
      return (
          <List component='ul' className={this.props.classes.list}>
            <ListItem key='circular_progress_animalList' component={CircularProgress}/>;
          </List>);
    }
  }
}

AnimalList.propTypes = {
  classes: PropTypes.object.isRequired,
  api: PropTypes.object.isRequired,
  ebus: PropTypes.object.isRequired,
};
export default withRouter(AnimalList);
