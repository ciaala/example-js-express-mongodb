import React from 'react';
import * as PropTypes from 'prop-types';
import AnimalForm from './AnimalForm';
import {withRouter} from 'react-router-dom';
import {CircularProgress} from '@material-ui/core';

class AnimalDetail extends React.Component {
  state = {
    animal: null,
  };

  loadAnimal(id) {
    this.props.api.get(id,
        (data) => this.setState({animal: data})
    );
  }

  componentDidMount() {
    const location = this.props.history.location.pathname;
    const tokens = location.split('/animals/');
    if (tokens.length >= 2) {
      this.loadAnimal(tokens[tokens.length - 1]);
    }
  }

  onUpdate(data) {
    data['_id'] = this.state.animal._id;
    this.props.ebus.publish({type: 'ANIMAL', action: 'UPDATE', data: data} );
  }

  render() {
    if (this.state.animal) {
      const {family, name, description} = this.state.animal;
      return (<AnimalForm
              buttonText='Update'
              family={family}
              name={name}
              description={description}
              onSubmit={this.onUpdate.bind(this)}/>
      );
    }
    return <CircularProgress/>;
  }

}

AnimalDetail.propTypes = {
  classes: PropTypes.object.isRequired,
  ebus: PropTypes.object.isRequired,
};
export default withRouter(AnimalDetail);
