import React from 'react';
import ReactDOM from 'react-dom';


import CrudApp from './CrudApp';

class Root extends React.Component {

  render() {
    return (
      <CrudApp></CrudApp>
    );
  }
};


ReactDOM.render( <Root/>,
    document.getElementById('root'),
);
