export class EventBus {
  constructor() {
    this.listenersMap = {};
  }

  publish({type, action, data}) {
    if (this.listenersMap.hasOwnProperty(type)
        && this.listenersMap[type].hasOwnProperty(action)) {
      const listeners = this.listenersMap[type][action]
      for ( let key of Object.getOwnPropertyNames(listeners) ) {
        const listener = listeners[key];
        listener(type, action, data);
      }
    }
  }

  subscribe(type, action, listener) {
    if (!this.listenersMap.hasOwnProperty(type)) {
      this.listenersMap[type] = {};
    }
    if (!this.listenersMap[type].hasOwnProperty(action)) {
      this.listenersMap[type][action] = {};
    }
    const key = Math.random().toString(16).substr(2,8);
    this.listenersMap[type][action][key] = listener;
    console.debug(`Listeners[${type}][${action}]:      `, Object.entries(this.listenersMap[type][action]));
    return key;
  }

  unsubscribe(type, action, listener) {
    if (this.listenersMap.hasOwnProperty(type) &&
        this.listenersMap[type].hasOwnProperty(action)) {
      const listeners = this.listenersMap[type][action];
      console.debug(`Listeners[${type}][${action}]:      `, listeners);
      delete listeners[listener];
      console.debug('Removed lister[${type}][${action}]: ', listeners);
    }
    console.debug('Trying to unregister', {type, action, listener});
  }
}
