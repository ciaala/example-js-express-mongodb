# React Crud Prototype

## Material-UI components used

- [AppBar](https://material-ui.com/components/app-bar/#bottom-app-bar)
- [List](https://material-ui.com/components/lists/#pinned-subheader-list)


## REACT Further documentation 

- [Conditional Rendering](https://www.robinwieruch.de/conditional-rendering-react/)
- [setState](https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component/)
- [Mounted/Unmounted Components](https://stackoverflow.com/questions/39767482/is-there-a-way-to-check-if-the-react-component-is-unmounted)
- [Material-UI Tables](https://material-ui.com/components/tables/#material-table)
- [React initial __State__ from  __Props__](https://stackoverflow.com/questions/40063468/react-component-initialize-state-from-props#40063494)


## Javascript Documentation

- [for...of](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of)
- [Array and how to remove elements](https://love2dev.com/blog/javascript-remove-from-array/)
- [Console log/debug/info](https://stackoverflow.com/questions/21876461/difference-between-console-log-and-console-debug#36355027) 
- [UUID in javascript](https://stackoverflow.com/a/2117523)

